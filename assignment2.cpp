#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int main() {
    int n;
    int x, y, z;
    cin >> n >> x >> y >> z;

    int xs = min(x, n / 5);
    int ys =min(y, (n - xs*5) / 2);
    int zs = min(z, (n - xs*5 - ys*2));

    if (n - xs * 5 - ys * 2 - zs * 2 > 0)
        cout << "Impossible!" << endl;
    else
        cout << xs + ys + zs << endl;
    return 0;
}
