#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int** g;
int* order;
int n;
int min_path = static_cast<int>(1e10);

int summarize_current() {
    int cur = 0;
    for (int i = 0; i < n; i++) {
        cur += g[order[i]][order[i+1]];
    }
    return cur;
}

bool used(int k) {
    for (int i = 0; i < n; i++)
        if (order[i] == k)
            return true;
    return false;
}

void process(int k) {
    if (k == n) {
        min_path = min(min_path, summarize_current());
    }

    for (int i = 0; i < n; i++) {
        if (!used(i)) {
            order[k] = i;
            process(k+1);
            order[k] = -1;
        }
    }
}

int main() {
    cin >> n;

    g = new int *[n];
    for (int i = 0; i < n; i++)
        g[i] = new int[n];

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cin >> g[i][j];
        }
    }


    order = new int[n+1];
    order[0] = order[n] = 0;
    for (int i = 1; i < n; i++)
        order[i] = -1;

    process(1);

    cout << min_path;


}
